import sys
from selectolax.lexbor import LexborHTMLParser


def replaceit(tree, tag, attribute, value_tuples):

    nodes = tree.css(f"{tag}[{attribute}]")
    for n in nodes:
        for h in value_tuples:
            if n.attrs[attribute].startswith(h[0].decode('utf-8')):
                new_attr = n.attrs[attribute]
                n.attrs[attribute] = new_attr.replace(
                    h[0].decode('utf-8'), h[1].decode('utf-8')
                )

    return tree


def reformattree(tree, hreftuples):
    replaceit(tree, "script", "src", hreftuples)
    replaceit(tree, "a", "href", hreftuples)
    replaceit(tree, "link", "href", hreftuples)

    return tree.html


def reformathtml(html, hreftuples):
    tree = get_tree(html)
    return reformattree(tree, hreftuples)


def get_file(fname):
    with open(fname, "r") as f:
        html = f.read()
    return html


def get_tree(html):
    tree = LexborHTMLParser(html)
    return tree


def add_header_scripts(tree, snippet):

    scriptree = LexborHTMLParser(snippet)
    scriptag = scriptree.tags("script")[0]
    headnode = tree.head
    last_child = headnode.last_child

    if last_child:
        last_child.insert_after(scriptag)

    return tree


def reformat_all(html, hreftuples, snippet):

    tree = get_tree(html)

    reformattree(tree, hreftuples)
    add_header_scripts(
        tree,
        snippet
    )
    return(tree.html)


def main():
    if len(sys.argv) == 0:
        fname = "-"
    else:
        fname = sys.argv[1]

    html = get_file(fname)
    tree = get_tree(html)

    htuples = [
        ("//s3.amazonaws.com/", "https://s3.amazonaws.com/"),
        ("//fonts.googleapis.com/", "https://fonts.googleapis.com/"),
        ("//fargo.io/", "https://s3.amazonaws.com/fargo.io/"),
        ("http://fargo.io/", "https://s3.amazonaws.com/fargo.io/"),
        ("//scripting.com/", "https://s3.amazonaws.com/scripting.com/"),
        ("http://scripting.com/", "https://s3.amazonaws.com/scripting.com/"),
        ("//radio3.io/", "https://s3.amazonaws.com/radio3.io/"),
        ("http://radio3.io/", "https://s3.amazonaws.com/radio3.io/"),
        ("//api.nodestorage.io/", "https://s3.amazonaws.com/api.nodestorage.io/"),
        ("http://api.nodestorage.io/", "https://s3.amazonaws.com/api.nodestorage.io/"),
    ]

    tree = reformattree(tree, htuples)
    tree = add_header_scripts(
        tree,
        '<script defer data-domain="blog.ncbt.org" src="https://plausible.io/js/script.js"></script>',
    )
    print(tree.html)


if __name__ == "__main__":
    main()
