import sys

from hihat.htmlrewrite import reformat_all

def rewrite(html, tuples, snippet):
    return reformat_all(html, tuples, snippet)

def main():
    if len(sys.argv) == 0:
        fname = "-"
    else:
        fname = sys.argv[1]

    html = get_file(fname)
    tree = get_tree(html)

    htuples = [
        ("//s3.amazonaws.com/", "https://s3.amazonaws.com/"),
        ("//fonts.googleapis.com/", "https://fonts.googleapis.com/"),
        ("//fargo.io/", "https://s3.amazonaws.com/fargo.io/"),
        ("http://fargo.io/", "https://s3.amazonaws.com/fargo.io/"),
        ("//scripting.com/", "https://s3.amazonaws.com/scripting.com/"),
        ("http://scripting.com/", "https://s3.amazonaws.com/scripting.com/"),
        ("//radio3.io/", "https://s3.amazonaws.com/radio3.io/"),
        ("http://radio3.io/", "https://s3.amazonaws.com/radio3.io/"),
        ("//api.nodestorage.io/", "https://s3.amazonaws.com/api.nodestorage.io/"),
        ("http://api.nodestorage.io/", "https://s3.amazonaws.com/api.nodestorage.io/"),
    ]

    tree = reformattree(tree, htuples)
    tree = add_header_scripts(
        tree,
        '<script defer data-domain="blog.ncbt.org" src="https://plausible.io/js/script.js"></script>',
    )
    print(tree.html)


if __name__ == "__main__":
    main()
