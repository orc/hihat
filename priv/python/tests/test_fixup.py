from hihat import htmlrewrite
import pytest
from selectolax.lexbor import LexborHTMLParser, LexborNode


def test_slashslash():
    html = '<html><head><title>Stuff</title></head><body><a href="//some/link/somewhere">Link</a></body></html>'
    html_replaced = '<html><head><title>Stuff</title></head><body><a href="http://some/link/somewhere">Link</a></body></html>'

    hrefs = [(b"//", b"http://")]

    tree = htmlrewrite.get_tree(html)
    result = htmlrewrite.reformattree(tree, hrefs)

    assert html_replaced == result


def test_plausible():
    html = '<html><head><title>Stuff</title><link rel="/asdf" type="css"></head><body><a href="//some/link/somewhere">Link</a></body></html>'
    html_replaced = '<html><head><title>Stuff</title><link rel="/asdf" type="css"><script src="hackerman"></script></head><body><a href="//some/link/somewhere">Link</a></body></html>'

    tree = htmlrewrite.get_tree(html)
    tree = htmlrewrite.add_header_scripts(tree, '<script src="hackerman"></script>')

    assert html_replaced == tree.html


def test_http_https():
    html = '<html><head><title>Stuff</title></head><body><a href="http://some/link/somewhere">Link</a></body></html>'
    html_replaced = '<html><head><title>Stuff</title></head><body><a href="https://some/link/somewhere">Link</a></body></html>'

    hrefs = [(b"http://", b"https://")]

    tree = htmlrewrite.get_tree(html)
    result = htmlrewrite.reformattree(tree, hrefs)

    assert html_replaced == result


def test_script_src():
    html = '<html><head><title>Nickelblog</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><script src="//fargo.io/code/editors/mediumeditor/dist/js/medium-editor.js"></script></head><body></body></html>'
    html_replaced = '<html><head><title>Nickelblog</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><script src="http://fargo.io/code/editors/mediumeditor/dist/js/medium-editor.js"></script></head><body></body></html>'

    hrefs = [(b"//", b"http://")]

    tree = htmlrewrite.get_tree(html)
    result = htmlrewrite.reformattree(tree, hrefs)

    assert html_replaced == result


def test_link_href():
    html = """<html><head><title>Nickelblog</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><script src="//fargo.io/code/editors/mediumeditor/dist/js/medium-editor.js"></script>
<link href="//scripting.com/code/includes/basic/styles.css" rel="stylesheet" type="text/css">
<link href="//radio3.io/code/pagestyles.css" rel="stylesheet" type="text/css">
</head><body></body></html>"""
    html_replaced = """<html><head><title>Nickelblog</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><script src="http://fargo.io/code/editors/mediumeditor/dist/js/medium-editor.js"></script>
<link href="http://scripting.com/code/includes/basic/styles.css" rel="stylesheet" type="text/css">
<link href="http://radio3.io/code/pagestyles.css" rel="stylesheet" type="text/css">
</head><body></body></html>"""

    hrefs = [(b"//", b"http://")]

    tree = htmlrewrite.get_tree(html)
    result = htmlrewrite.reformattree(tree, hrefs)

    assert html_replaced == result
