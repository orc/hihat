import pytest

@pytest.fixture(scope="session")
def htmlfromfile():
    with open("files/blog.html") as f:
        return f.read()
