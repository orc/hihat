defmodule Hihat.PythonHelper do
  @moduledoc """
  Helper function to call out to Python
  """

  use GenServer
  require Logger

  @htuples [
    {"//s3.amazonaws.com/", "https://s3.amazonaws.com/"},
    {"http://s3.amazonaws.com/", "https://s3.amazonaws.com/"},
    {"//fonts.googleapis.com/", "https://fonts.googleapis.com/"},
    {"//fargo.io/", "https://s3.amazonaws.com/fargo.io/"},
    {"http://fargo.io/", "https://s3.amazonaws.com/fargo.io/"},
    {"//scripting.com/", "https://s3.amazonaws.com/scripting.com/"},
    {"http://scripting.com/", "https://s3.amazonaws.com/scripting.com/"},
    {"//radio3.io/", "https://s3.amazonaws.com/radio3.io/"},
    {"http://radio3.io/", "https://s3.amazonaws.com/radio3.io/"},
    {"//api.nodestorage.io/", "https://s3.amazonaws.com/api.nodestorage.io/"},
    {"http://api.nodestorage.io/", "https://s3.amazonaws.com/api.nodestorage.io/"}
  ]

  @plausible "<script defer data-domain=\"blog.ncbt.org\" src=\"https://plausible.io/js/script.js\"></script>"

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def format(html) do
    GenServer.call(Hihat.PythonHelper, {:format, html})
  end

  @impl true
  def init(_ok) do
    :ets.new(:rewritten_html, [:set, :protected, :named_table])
    pid = start_python()
    {:ok, %{:pid => pid |> elem(1)}}
  end

  @impl true
  def handle_call({:format, html}, _from, state) do
    final_html = html_url_rewrite(html, state)
    {:reply, final_html, state}
  end

  @impl true
  def terminate(_reason, state) do
    :python.stop(state[:pid])
  end

  defp start_python(version \\ 'python3') do
    :python.start([
      {:python_path,
       [Application.app_dir(:hihat), "priv", "python"] |> Path.join() |> to_charlist()},
      {:python, version}
    ])
  end

  defp call(pid, mod, fun, arg) do
    :python.call(pid, mod, fun, arg)
  end

  defp cachecheck([], html, state) do
    # path = [:code.priv_dir(:hihat), "python"] |> Path.join()

    rewritten =
      call(state[:pid], :htmlhelper, :rewrite, [html, @htuples, @plausible])
      |> List.to_string()

    hash =
      :crypto.hash(:sha, html)
      |> Base.encode16()
      |> String.downcase()

    :ets.insert(:rewritten_html, {hash, rewritten})

    rewritten
  end

  defp cachecheck([{_hash, rewritten} | _tail], _html, _state) do
    rewritten
  end

  def html_url_rewrite(html, state) do
    hash =
      :crypto.hash(:sha, html)
      |> Base.encode16()
      |> String.downcase()

    :ets.lookup(:rewritten_html, hash)
    |> cachecheck(html, state)
  end
end
