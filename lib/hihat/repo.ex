defmodule Hihat.Repo do
  use Ecto.Repo,
    otp_app: :hihat,
    adapter: Ecto.Adapters.Postgres
end
