defmodule Hihat.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Hihat.Repo,
      # Start the Telemetry supervisor
      HihatWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Hihat.PubSub},
      # Start the Endpoint (http/https)
      HihatWeb.Endpoint,
      # Start a worker by calling: Hihat.Worker.start_link(arg)
      # {Hihat.Worker, arg}
      {Hihat.PythonHelper, name: PythonHelper}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Hihat.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    HihatWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
