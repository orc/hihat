defmodule HihatWeb.PageController do
  use HihatWeb, :controller

  def index(conn, _params) do
    target =
      if conn.request_path == "/" do
        "/index.html"
      else
        conn.request_path
      end

    case HTTPoison.get("https://s3.amazonaws.com/oldschool.scripting.com/croldham#{target}") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        reformatted = Hihat.HtmlUtils.html_url_rewrite(body)
        html(conn, reformatted)

      {:ok, %HTTPoison.Response{status_code: 403}} ->
        conn |> Phoenix.Controller.render(HihatWeb.ErrorView, :"404")

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        conn |> Phoenix.Controller.render(HihatWeb.ErrorView, :"404")

      {:error, %HTTPoison.Error{reason: _reason}} ->
        IO.puts("Error")
        conn |> Phoenix.Controller.render(HihatWeb.ErrorView, :"500")
    end
  end
end
