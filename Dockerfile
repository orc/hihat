ARG BUILDER_IMAGE="hexpm/elixir:1.13.4-erlang-25.0.2-ubuntu-focal-20211006"
ARG RUNNER_IMAGE="ubuntu:focal"

FROM ${BUILDER_IMAGE} as builder

RUN apt-get update -y \
  && apt-get -y dist-upgrade \
  && apt-get install -y git python3 python3-pip curl \
  && apt-get clean && rm -f /var/lib/apt/lists/*_*

RUN curl -o /tmp/lexbor_signing.key https://lexbor.com/keys/lexbor_signing.key
RUN cd /tmp && apt-key add lexbor_signing.key && rm /tmp/lexbor_signing.key
RUN echo "deb https://packages.lexbor.com/ubuntu/ focal liblexbor" > /etc/apt/sources.list.d/lexbor.list
RUN apt -y update && apt -y install liblexbor

# install build dependencies

RUN pip3 install selectolax

# prepare build dir
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
  mix local.rebar --force

# set build ENV
ENV MIX_ENV="prod"

# install mix dependencies
COPY mix.exs mix.lock ./
RUN mix deps.get --only $MIX_ENV
RUN mkdir config

# copy compile-time config files before we compile dependencies
# to ensure any relevant config change will trigger the dependencies
# to be re-compiled.
COPY config/appsignal.exs config/config.exs config/${MIX_ENV}.exs config/
RUN mix deps.compile

COPY priv priv

# note: if your project uses a tool like https://purgecss.com/,
# which customizes asset compilation based on what it finds in
# your Elixir templates, you will need to move the asset compilation
# step down so that `lib` is available.
COPY assets assets

# compile assets
RUN mix assets.deploy

# Compile the release
COPY lib lib

RUN mix compile

# Changes to config/runtime.exs don't require recompiling the code
COPY config/runtime.exs config/
COPY config/appsignal.exs config/

COPY rel rel
RUN mix release

# start a new build stage so that the final image will only contain
# the compiled release and other runtime necessities
FROM ${RUNNER_IMAGE}

# This is a little repetitive from above, to avoid a more ham-handed "COPY / /" below
RUN apt-get update -y \
  && apt-get -y dist-upgrade \
  && apt-get install -y git python3 python3-pip curl libstdc++6 openssl libncurses5 locales \
  && apt-get clean && rm -f /var/lib/apt/lists/*_*

RUN curl -o /tmp/lexbor_signing.key https://lexbor.com/keys/lexbor_signing.key
RUN cd /tmp && apt-key add lexbor_signing.key && rm /tmp/lexbor_signing.key
RUN echo "deb https://packages.lexbor.com/ubuntu/ focal liblexbor" > /etc/apt/sources.list.d/lexbor.list
RUN apt -y update && apt -y install liblexbor

# install build dependencies

RUN pip3 install selectolax

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

WORKDIR "/app"
RUN chown nobody /app

# Only copy the final release from the build stage
COPY --from=builder --chown=nobody:root /app/_build/prod/rel/hihat ./

USER nobody

CMD ["/app/bin/migrate-and-start"]

# Appended by flyctl
ENV ECTO_IPV6 true
ENV ERL_AFLAGS "-proto_dist inet6_tcp"
