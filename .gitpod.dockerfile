FROM gitpod/workspace-postgres

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV PATH="$HOME/.asdf/bin:$HOME/.asdf/shims:$PATH"

RUN curl -O https://lexbor.com/keys/lexbor_signing.key
RUN apt-key add lexbor_signing.key

RUN echo "deb http://packages.lexbor.com/ubuntu/ focal liblexbor" > /etc/apt/sources.list.d/lexbor.list

# Install asdf, asdf-erlang and asdf-nodejs dependencies.
RUN apt-get update \
    && apt -y full-upgrade \
    && apt-get -y install curl git liblexbor \
    && apt-get -y install build-essential autoconf m4 libncurses5-dev libwxgtk3.0-gtk3-dev libgl1-mesa-dev libglu1-mesa-dev libpng-dev libssh-dev unixodbc-dev xsltproc fop libxml2-utils libncurses-dev openjdk-11-jdk \
    && apt-get -y install dirmngr gpg curl gawk

# Install inotify-tools for Phoenix Live Reloading support
# @see https://hexdocs.pm/phoenix/installation.html#inotify-tools-for-linux-users
RUN apt-get -y install inotify-tools

# Install asdf
RUN git clone https://github.com/asdf-vm/asdf.git "$HOME/.asdf" --branch v0.9.0

# Add asdf erlang, elixir and node plugins
RUN asdf plugin-add erlang \
    && asdf plugin-add elixir

COPY .tool-versions $HOME/

# Install erlang, elixir and node and remove left over files
RUN asdf install \
    && rm -rf  /tmp/*

RUN pip3 install selectolax

# Allow gitpod group to edit .asdf directory
RUN true \
    && chown -R root:gitpod $HOME/.asdf \
    && chmod -R g+rw $HOME/.asdf
